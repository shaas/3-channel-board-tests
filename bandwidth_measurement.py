#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to measure the bandwidth properties of each of the three DQQDIDT
channels using the automated measurement framework.

General idea:
    Generate a frequency with the function generator, record it with the three channel board
    and then measure the amplitude in the data.
    Afterwards the -3dB point is determined and plotted.

It uses the functions and classes of the automated HW testing framework:
    https://gitlab.cern.ch/qps/automated_hw_testing
and the UQDS data analysis toolkit:
    https://gitlab.cern.ch/qps/uqds-data-analysis-scripts

@author: Severin Haas at CERN, March 2020
"""
# pylint: disable=C0103

import time
import serial
import matplotlib.pyplot as plt
import numpy as np
import pyvisa
from lib.uqds import UQDS
from lib.keysight_33500 import Keysight33500
import datahandling.signal_analysis as signal


####################### Test Parameters #######################################
dut_name = "3 Channel Board v1.0"
test_name = "Bandwidth Test"
board_number = 1
channel_number = 1
path = 'C:/Users/qpsop/cernbox_shaas/Win_Sync/3channel_test/'
bandwidth_filename = 'dqqdidt_board' + str(board_number) + '_bw_ch' + \
                      str(channel_number) + '.png'

# Define the address for reading the buffer
address_start = 96
address_stop = 100
samples = 8000

# Board settings
if channel_number == 1:
    channel_name = "Ch. A"
    channel_range = 4 # Volt
    amplitude = 4 # Volt
    dqqdidt_lsb = 2**-24 / 2.5
if channel_number == 2:
    channel_name = "Ch. B"
    channel_range = 5 # Volt
    amplitude = 5 # Volt
    dqqdidt_lsb = 2**-24
if channel_number == 3:
    channel_name = "Ch. C"
    channel_range = 5 # Volt
    amplitude = 5 # Volt
    dqqdidt_lsb = 2**-24

clock = 40.0e+6
adc_speed = 500e+3
adc_set_value = int(clock / adc_speed)
sample_rate = clock / adc_set_value
f_resolution = sample_rate / samples

# Measurement settings
freq_steps = 20
start_freq_base = 2 # 10^n
stop_freq_base = 6 # 10^n

# Define the COM port to use (see device manager setting on windows)
com = 'COM22'

# USB ID of the signal generator
sig_generator_id = 'USB0::0x0957::0x2807::MY52401786::INSTR'


####################### Setup #################################################
# Create port for communication
ser = serial.Serial()

# Instanciate an UART communication object with the board
board_obj = UQDS(ser, com)

# Signal generator
rm = pyvisa.ResourceManager()
sig_gen_handler = rm.open_resource(sig_generator_id)
function_generator = Keysight33500(sig_gen_handler)
function_generator.set_high_impedance(1)
function_generator.set_output_on(1)

# Frequency list setup
frequencies = np.logspace(start_freq_base, stop_freq_base, freq_steps)

# Recalculate the frequencies that they are integer multiples of df to reduce amplitude errors
for s in range(0, len(frequencies)):
    frequencies[s] = (frequencies[s] // f_resolution) * f_resolution


####################### Mesurements ###########################################
ampl_list = np.array([])
ampl_list_db = np.array([])
total_freq_steps = len(frequencies)
n = 0

for freq in frequencies:
    n = n + 1

    # Generate new signal
    function_generator.set_sine(1, freq, amplitude, 0.0)

    board_obj.reset()
    time.sleep(4)


    ####################### Loading new parameters ############################
    # Set ADC speed
    board_obj.write_register(12, [0x00, 0x00, 0x00, adc_set_value])
    time.sleep(10e-3)

    # Set readout config
    board_obj.write_register(10, [0x64, 0x00, 0x00, 0x00])
    time.sleep(10e-3)

    # Setting all channel config registers to zero to get unfiltered and undecimated data
    for i in range(19, 34):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)

    for i in range(128, 139):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)

    time.sleep(1)


    ####################### Acquiring data ####################################
    # Trigger the buffer
    board_obj.write_register(0x00, [0x13, 0x00, 0x00, 0x00])

    # Dummy reads, otherwise the buffer is not triggered correctly
    time.sleep(3)
    for i in range(0, 200):
        data = board_obj.read_register(i, 5)

    time.sleep(3)
    for i in range(0, 200):
        data = board_obj.read_register(i, 5)

        if i == 12:
            set_sps = data

    print(set_sps)

    # Read the buffer
    buffer = board_obj.read_buffer(samples, address_start, address_stop)
    # buffer = board_obj.trig_and_read_buffer(samples, adress_start, adress_stop, system_flag=0xea)

    # Multiply buffer values with LSB value
    buffer = np.multiply(buffer, dqqdidt_lsb)


    ####################### Starting data analysis ############################
    num_samples = np.size(buffer[:, channel_number - 1])
    window = np.hanning(num_samples)
    buffer_window = buffer[:, channel_number - 1] * window

    [f_meas, ampl, ampl_db, _] = signal.calculate_signal_parameters(buffer_window,
                                                                    sample_rate,
                                                                    channel_range,
                                                                    amplitude,
                                                                    freq)

    # Save data for later analysis
    ampl_list = np.append(ampl_list, ampl)
    ampl_list_db = np.append(ampl_list_db, ampl_db)

    # Some nice output for the user
    done_percent = int(100 * (n / total_freq_steps))
    print("Measurement " + str(n) + "/" + str(total_freq_steps) + \
          " (" + str(done_percent) + "%)")
    print("Set f: %.3f" % (freq / 1e+3) + " kHz")
    print("Meas. f: %.3f" % (f_meas / 1e+3) + " kHz")
    print("Amplitude: %.3f" % ampl + " V")
    print("Amplitude: %.3f" % ampl_db + " dBFs")
    print("---------------------------")


# Close the connection to the physical devices
ser.close()
sig_gen_handler.close()


# Find the low pass -3 dB point
value_3db = ampl_list_db[0] - 3.0
freq_3db = signal.find_3db_point(frequencies, ampl_list_db, value_3db)
print("-3 dB point at %.3f" % (freq_3db / 1e+3) + " kHz")


####################### Plot ##################################################
plt.figure("Amplitude Plot in dB")
plt.semilogx(frequencies, ampl_list_db, marker='o')
x_lim = plt.gca().get_xlim()
y_lim = plt.gca().get_ylim()
plt.xlim(x_lim)
plt.ylim(y_lim)

# Create the -3 dB lines
x_axis_line_x = [freq_3db, freq_3db]
x_axis_line_y = [y_lim[0], value_3db]
plt.semilogx(x_axis_line_x, x_axis_line_y, '--', color='#929591')

y_axis_line_x = [x_lim[0], freq_3db]
y_axis_line_y = [value_3db, value_3db]
plt.semilogx(y_axis_line_x, y_axis_line_y, '--', color='#929591')
plt.grid(True)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude (dBfs)')
plt.title(test_name + ", " + channel_name)
plt.tight_layout()
plt.savefig(path + bandwidth_filename)
