# 3 Channel Board Tests

Please note: this repository is no longer maintained, as it was moved to [https://gitlab.cern.ch/qps/dqqdidt-board-test](https://gitlab.cern.ch/qps/dqqdidt-board-test)

***

This repository contains several scripts to test the newly developed 3 channel board (DQQDIDT).
It uses the Automated Test Framework (ATF) libraries to control measurement equipment or using signal analysis functions.
The scripts in this repository are written for an adopted <abbr title="Universial Quench Detection System">UQDS</abbr> [Firmware](https://gitlab.cern.ch/qps/dqqdidt-hdl-test). Therefore it uses the serial communication interface to the board and as a serial to USB converter a FTDI UM232H was used.
To do that, the following lines to the FPGA were used:

| Name FTDI side | Name FPGA side | Pin number J1            |
| ---------------| ---------------| -------------------------|
| Tx FTDI        | Rx (SPI_SCK)   | A14                      |
| Rx FTDI        | Tx (SPI_MISO)  | A12                      |
| GND            | GND            | e.g. C2                  |
| SPI nCS        | GND            | C14, put to GND, e.g. C2 |


## Necessary Python Packages
To run, the scripts only need a few python packages:
  * time
  * serial
  * matplotlib.pyplot
  * numpy
  * pyvisa
  * scipy
  * In one case also sys and winsound, but this is more a nice to have than a must, can be uncommented.

If they are not availble, they shall be installed, e.g. via pip:
```
pip install numpy matplotlib scipy pyserial pyvisa
```
Please also check, which dependecies are needed when using the <abbr title="Automated Test Framework">ATF</abbr>: [https://gitlab.cern.ch/qps/automated_hw_testing](https://gitlab.cern.ch/qps/automated_hw_testing).
