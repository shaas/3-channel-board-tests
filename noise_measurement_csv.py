#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to measure the noise properties of each of the three DQQDIDT
channels using the automated measurement framework.
This script reads a given csv-file which contains the measurements.

General idea:
    - Short the input of the channel and record a buffer
    - Analyze the noise levels:
        - peak-peak noise
        - standard deviation
        - main frequency components

It uses the functions and classes of the automated HW testing framework:
    https://gitlab.cern.ch/qps/automated_hw_testing
and the UQDS data analysis toolkit:
    https://gitlab.cern.ch/qps/uqds-data-analysis-scripts

@author: Severin Haas at CERN, March 2020
"""
# pylint: disable=C0103

import numpy as np
import matplotlib.pyplot as plt
import lib.signal_analysis as noise
import lib.dsp_functions as dsp


####################### Test Parameters #######################################
dut_name = "3 Channel Board v1.0"
test_name = "Bandwidth Test"
run = 4
filename = '/Users/sev/cernbox/Win_Sync/3channel_test/test_run' + str(run) + '.csv'

# Number of samples per analysis
samples = 8150

# UQDS settings
uqds_lsb = 2**-24
adc_set_value = int(0xc3)
sample_rate = 40e+6 / adc_set_value
f_resolution = sample_rate / samples
channel_a_range = 20 # +-10 Volt
channel_range = 50 # +- 25 Volt
channel_a_lsb = channel_a_range / (2**20)
channel_lsb = channel_range / (2**20)


# Multiplication factor to show e.g. uV instead of V
mult = int(1e+6)
if mult == 1:
    val = " V"
elif mult == int(1e+3):
    val = " mV"
elif mult == int(1e+6):
    val = " uV"


####################### Load Data from File ###################################
data_ch1 = np.loadtxt(filename, delimiter=',', skiprows=1, usecols=1, max_rows=samples)
data_ch2 = np.loadtxt(filename, delimiter=',', skiprows=1, usecols=3, max_rows=samples)
data_ch3 = np.loadtxt(filename, delimiter=',', skiprows=1, usecols=5, max_rows=samples)


####################### Data analysis #########################################
print("Used n = " + str(samples) + " Samples for analysis. LSB = %.2f\n" % channel_lsb)

# Mean value
ch1_mean = noise.calculate_mean(data_ch1)
ch2_mean = noise.calculate_mean(data_ch2)
ch3_mean = noise.calculate_mean(data_ch3)

# Standard deviation
ch1_std_dev = noise.calculate_std_dev(data_ch1)
ch2_std_dev = noise.calculate_std_dev(data_ch2)
ch3_std_dev = noise.calculate_std_dev(data_ch3)
ch1_std_dev_lsb = ch1_std_dev / channel_a_lsb
ch2_std_dev_lsb = ch2_std_dev / channel_lsb
ch3_std_dev_lsb = ch3_std_dev / channel_lsb

print("Ch1: %.3f" % np.multiply(ch1_mean, mult) + val + \
      " +- %.3f" % np.multiply(ch1_std_dev, mult)+ \
      " (%.2f" % ch1_std_dev_lsb + " LSB)")
print("Ch2: %.3f" % np.multiply(ch2_mean, mult) + val + \
      " +- %.3f" % np.multiply(ch2_std_dev, mult) + \
      " (%.2f" % ch2_std_dev_lsb + " LSB)")
print("Ch3: %.3f" % np.multiply(ch3_mean, mult) + val + \
      " +- %.3f" % np.multiply(ch3_std_dev, mult)+ \
      " (%.2f" % ch3_std_dev_lsb + " LSB)\n")

# Peak-Peak noise
ch1_pp = noise.calculate_pp_noise(data_ch1)
ch2_pp = noise.calculate_pp_noise(data_ch2)
ch3_pp = noise.calculate_pp_noise(data_ch3)
ch1_pp_lsb = noise.calculate_pp_noise(data_ch1) / channel_a_lsb
ch2_pp_lsb = noise.calculate_pp_noise(data_ch2) / channel_lsb
ch3_pp_lsb = noise.calculate_pp_noise(data_ch3) / channel_lsb

print("Ch1 pp noise: %.3f" % np.multiply(ch1_pp, mult) + val + ", %.2f" % ch1_pp_lsb + " LSB")
print("Ch2 pp noise: %.3f" % np.multiply(ch2_pp, mult) + val + ", %.2f" % ch2_pp_lsb + " LSB")
print("Ch3 pp noise: %.3f" % np.multiply(ch3_pp, mult) + val + ", %.2f" % ch3_pp_lsb + " LSB")

# Plot a histogram
noise.plot_hist(data_ch1 * mult, 20, 'Ch1', 'Voltage', val)
noise.plot_hist(data_ch2 * mult, 20, 'Ch2', 'Voltage', val)
noise.plot_hist(data_ch3 * mult, 20, 'Ch3', 'Voltage', val)


####################### FFT analysis ##########################################
# Frequency components
num_fft = len(data_ch1)
window = np.hanning(num_fft)

# FFT
[ch1_fft_x, ch1_fft_y] = dsp.calculate_fft(data_ch1 * window, sample_rate)
[ch2_fft_x, ch2_fft_y] = dsp.calculate_fft(data_ch2 * window, sample_rate)
[ch3_fft_x, ch3_fft_y] = dsp.calculate_fft(data_ch3 * window, sample_rate)

y_fft_abs1 = np.abs(ch1_fft_y[0:num_fft//2])
y_fft_scaled1 = 2.0 / num_fft * y_fft_abs1 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs1 = 20 * np.log10(y_fft_scaled1 / 10)

y_fft_abs2 = np.abs(ch2_fft_y[0:num_fft//2])
y_fft_scaled2 = 2.0 / num_fft * y_fft_abs2 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs2 = 20 * np.log10(y_fft_scaled2 / 10)

y_fft_abs3 = np.abs(ch3_fft_y[0:num_fft//2])
y_fft_scaled3 = 2.0 / num_fft * y_fft_abs3 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs3 = 20 * np.log10(y_fft_scaled3 / 10)

plt.figure()
plt.subplot(131)
plt.plot(ch1_fft_x / 1e+3, y_fft_dbfs1, label="Ch1")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)

plt.subplot(132)
plt.plot(ch2_fft_x / 1e+3, y_fft_dbfs2, label="Ch2")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)

plt.subplot(133)
plt.plot(ch3_fft_x / 1e+3, y_fft_dbfs3, label="Ch3")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)
