#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 17:20:41 2020

Script to measure the noise properties with different decimation levels using the
automated measurement framework.

General idea:
    - Short the input of the channel and record a buffer
    - Set decimation level
    - Analyze the noise levels:
        - peak-peak noise
        - standard deviation
        - main frequency components
    - Plot data and save it

It uses the functions and classes of the automated HW testing framework:
    https://gitlab.cern.ch/qps/automated_hw_testing

@author: Severin Haas at CERN, April 2020
"""
# pylint: disable=C0103

import time
import numpy as np
import serial
import matplotlib.pyplot as plt
from lib.uqds3 import UQDS
import datahandling.signal_analysis as noise

start_time = time.time()

####################### Test Parameters #######################################
dut_name = "3 Channel Board v1.0"
board_number = 1
save_plot = False
path = 'C:/Users/qpsop/cernbox_shaas/Win_Sync/3channel_test/'
image_filename = 'dqqdidt_board' + str(board_number) + '_dec_analysis_float_'

# Define the address for reading the buffer
adress_start = 96
adress_stop = 100
# sample_buffer = 8000
samples = 8000

# UQDS settings
clock = 40.0e+6
dqqdidt_lsb = 2**-24
dqqdidt_a_lsb = 2**-24 / 2.5 # channel A
adc_speed = 500e+3
adc_set_value = int(clock / adc_speed)
sample_rate = clock / adc_set_value
f_resolution = sample_rate / samples

# Table contains all decimation values which shall be tested
dec_table = [0, 4, 8, 16, 32, 64, 256]

channel_a_range = 20 # +-10 Volt
channel_range = 50 # +- 25 Volt
channel_a_lsb = channel_a_range / (2**20)
channel_lsb = channel_range / (2**20)

# FFT plot area
fft_ylim = [-155, -100]

hist_bins = 20

# Define the COM port to use (see device manager setting on windows)
com = 'COM22'

# Multiplication factor to show e.g. uV instead of V
mult = int(1e+3)
if mult == 1:
    val = "V"
elif mult == int(1e+3):
    val = "mV"
elif mult == int(1e+6):
    val = "uV"


####################### Setup #################################################
# Create port for communication
ser = serial.Serial()

# Instanciate an UART communication object with UQDS class
board_obj = UQDS(ser, com)


# Numpy arrays to store measurement results
ch1_std_dev_lsb_values = np.array([])
ch2_std_dev_lsb_values = np.array([])
ch3_std_dev_lsb_values = np.array([])
ch1_std_dev_values = np.array([])
ch2_std_dev_values = np.array([])
ch3_std_dev_values = np.array([])
ch1_pp_values = np.array([])
ch2_pp_values = np.array([])
ch3_pp_values = np.array([])
ch1_pp_lsb_values = np.array([])
ch2_pp_lsb_values = np.array([])
ch3_pp_lsb_values = np.array([])


for dec_value in dec_table:
    print("Decimation level: " + str(dec_value))

    ####################### Load Parameters #######################################
    board_obj.reset()
    time.sleep(5)
    print("Reset done")

    # Set ADC speed
    if adc_set_value < 255:
        board_obj.write_register(12, [0x00, 0x00, 0x00, adc_set_value])
    else:
        adc_set_value_msb = int(adc_set_value / 65536)
        new_adc_set_value = int(adc_set_value - adc_set_value_msb * 65536)
        adc_set_value_lsb2 = int(new_adc_set_value / 256)
        adc_set_value_lsb1 = int(new_adc_set_value - adc_set_value_lsb2 * 256)
        board_obj.write_register(12, [0x00, adc_set_value_msb, adc_set_value_lsb2,
                                      adc_set_value_lsb1])
    time.sleep(10e-3)

    # Set readout config
    board_obj.write_register(10, [0x64, 0x00, 0x00, 0x00])
    time.sleep(10e-3)

    for i in range(19, 34):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)

    for i in range(128, 139):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)


    # Check which decimation value shall be used
    if dec_value == 0:
        dec_setting = 0x00
    elif dec_value == 4:
        dec_setting = 0x02
    elif dec_value == 8:
        dec_setting = 0x04
    elif dec_value == 16:
        dec_setting = 0x06
    elif dec_value == 32:
        dec_setting = 0x08
    elif dec_value == 64:
        dec_setting = 0x0A
    elif dec_value == 256:
        dec_setting = 0x0C

    # Set new decimation
    board_obj.write_register(128, [0x00, 0x00, 0x00, dec_setting])
    time.sleep(10e-3)
    board_obj.write_register(132, [0x00, 0x00, 0x00, dec_setting])
    time.sleep(10e-3)
    board_obj.write_register(136, [0x00, 0x00, 0x00, dec_setting])
    time.sleep(10e-3)

    time.sleep(4)
    print("New config done")


    ####################### Aquiring data #########################################
    data_ch1 = np.array([])
    data_ch2 = np.array([])
    data_ch3 = np.array([])

    board_obj.write_register(0x00, [0x13, 0x00, 0x00, 0x00])
    print("Board triggered")

    time.sleep(6)

    for n in range(0, 2):
        for i in range(0, 200):
            data = board_obj.read_register(i, 5)

            if i == 7:
                print("System status: " + str(data))

        time.sleep(20e-3)

    set_sps = board_obj.read_register(12, 5)

    buffer = board_obj.read_buffer(samples, adress_start, adress_stop)

    data_ch1 = np.multiply(buffer[:, 0], dqqdidt_a_lsb)
    data_ch2 = np.multiply(buffer[:, 1], dqqdidt_lsb)
    data_ch3 = np.multiply(buffer[:, 2], dqqdidt_lsb)


    ####################### Noise analysis ########################################
    print("Used n = " + str(len(data_ch1)) + " samples for analysis.")
    set_adc_value = int.from_bytes(set_sps[0:4], byteorder='big', signed=True)
    sample_rate = (clock / int(set_adc_value))
    print("fs = %.3f" % (sample_rate / 1e+3) + " kHz")

    # Mean value
    ch1_mean = noise.calculate_mean(data_ch1)
    ch2_mean = noise.calculate_mean(data_ch2)
    ch3_mean = noise.calculate_mean(data_ch3)

    # Standard deviation
    ch1_std_dev = noise.calculate_std_dev(data_ch1)
    ch2_std_dev = noise.calculate_std_dev(data_ch2)
    ch3_std_dev = noise.calculate_std_dev(data_ch3)
    ch1_std_dev_lsb = ch1_std_dev / channel_a_lsb
    ch2_std_dev_lsb = ch2_std_dev / channel_lsb
    ch3_std_dev_lsb = ch3_std_dev / channel_lsb

    print("Ch1: %.3f" % np.multiply(ch1_mean, mult) + " " + val + \
          " +- %.3f" % np.multiply(ch1_std_dev, mult)+ \
          " (%.2f" % ch1_std_dev_lsb + " LSB)")
    print("Ch2: %.3f" % np.multiply(ch2_mean, mult) + " " + val + \
          " +- %.3f" % np.multiply(ch2_std_dev, mult) + \
          " (%.2f" % ch2_std_dev_lsb + " LSB)")
    print("Ch3: %.3f" % np.multiply(ch3_mean, mult) + " " + val + \
          " +- %.3f" % np.multiply(ch3_std_dev, mult)+ \
          " (%.2f" % ch3_std_dev_lsb + " LSB)\n")

    # Peak-Peak noise
    ch1_pp = noise.calculate_pp_noise(data_ch1)
    ch2_pp = noise.calculate_pp_noise(data_ch2)
    ch3_pp = noise.calculate_pp_noise(data_ch3)
    ch1_pp_lsb = ch1_pp / channel_a_lsb
    ch2_pp_lsb = ch2_pp / channel_lsb
    ch3_pp_lsb = ch3_pp / channel_lsb
    print("Ch1 pp noise: %.3f" % np.multiply(ch1_pp, mult) + val + \
          ", %.2f" % ch1_pp_lsb + " LSB")
    print("Ch2 pp noise: %.3f" % np.multiply(ch2_pp, mult) + val + \
          ", %.2f" % ch2_pp_lsb + " LSB")
    print("Ch3 pp noise: %.3f" % np.multiply(ch3_pp, mult) + val + \
          ", %.2f" % ch3_pp_lsb + " LSB")

    # Store data for analysis
    ch1_std_dev_values = np.append(ch1_std_dev_values, ch1_std_dev)
    ch2_std_dev_values = np.append(ch2_std_dev_values, ch2_std_dev)
    ch3_std_dev_values = np.append(ch3_std_dev_values, ch3_std_dev)
    ch1_std_dev_lsb_values = np.append(ch1_std_dev_lsb_values, ch1_std_dev_lsb)
    ch2_std_dev_lsb_values = np.append(ch2_std_dev_lsb_values, ch2_std_dev_lsb)
    ch3_std_dev_lsb_values = np.append(ch3_std_dev_lsb_values, ch3_std_dev_lsb)

    ch1_pp_values = np.append(ch1_pp_values, ch1_pp)
    ch2_pp_values = np.append(ch2_pp_values, ch2_pp)
    ch3_pp_values = np.append(ch3_pp_values, ch3_pp)
    ch1_pp_lsb_values = np.append(ch1_pp_lsb_values, ch1_pp_lsb)
    ch2_pp_lsb_values = np.append(ch2_pp_lsb_values, ch2_pp_lsb)
    ch3_pp_lsb_values = np.append(ch3_pp_lsb_values, ch3_pp_lsb)

    print("-----------------------------------\n")


# Close connection
ser.close()


####################### Plotting data #########################################
plt.figure()
plt.plot(dec_table, ch1_std_dev_lsb_values, '--*', label='Ch1')
plt.plot(dec_table, ch2_std_dev_lsb_values, '--*', label='Ch2')
plt.plot(dec_table, ch3_std_dev_lsb_values, '--*', label='Ch3')
plt.xlabel('Decimation level')
plt.ylabel('Std. Dev. (LSB)')
plt.xlim([dec_table[0] - 2, dec_table[len(dec_table) - 1] + 2])
plt.legend()
plt.tight_layout()
plt.grid(True)
if save_plot:
    plt.savefig(path + image_filename + 'std_dev_lsb.png')


plt.figure()
plt.plot(dec_table, ch1_pp_values * mult, '--*', label='Ch1')
plt.plot(dec_table, ch2_pp_values * mult, '--*', label='Ch2')
plt.plot(dec_table, ch3_pp_values * mult, '--*', label='Ch3')
plt.xlabel('Decimation level')
plt.ylabel('Peak-Peak Noise (' + val + ')')
plt.xlim([dec_table[0] - 2, dec_table[len(dec_table) - 1] + 2])
plt.legend()
plt.tight_layout()
plt.grid(True)
if save_plot:
    plt.savefig(path + image_filename + 'pp.png')


plt.figure()
plt.plot(dec_table, ch1_pp_lsb_values, '--*', label='Ch1')
plt.plot(dec_table, ch1_pp_lsb_values, '--*', label='Ch2')
plt.plot(dec_table, ch1_pp_lsb_values, '--*', label='Ch3')
plt.xlabel('Decimation level')
plt.ylabel('Peak-Peak Noise (std. dev. LSB)')
plt.xlim([dec_table[0] - 2, dec_table[len(dec_table) - 1] + 2])
plt.legend()
plt.tight_layout()
plt.grid(True)
if save_plot:
    plt.savefig(path + image_filename + 'pp_lsb.png')
