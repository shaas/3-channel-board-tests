#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to measure the linearity of each of the three DQQDIDT
channels using the automated measurement framework.

General idea:
    Generate a precise voltage, measure it with a DMM and the board itself.

It uses the functions and classes of the automated HW testing framework:
    https://gitlab.cern.ch/qps/automated_hw_testing

@author: Severin Haas at CERN, April 2020
"""
# pylint: disable=C0103

import time
from sys import platform
import serial
import matplotlib.pyplot as plt
import numpy as np
from lib.uqds import UQDS
from lib.pdvs2 import PDVS2

win_os = False
if platform == "win32":
    win_os = True
    import winsound


####################### Test Parameters #######################################
dut_name = "3 Channel Board v1.0"
test_name = "Linearity Test"
board_number = 1
channel_number = 1
path = 'C:/Users/qpsop/cernbox_shaas/Win_Sync/3channel_test/'
linearity_filename = 'dqqdidt_board' + str(board_number) + '_linearity_ch' + \
                      str(channel_number) + '_bipolar.png'

if channel_number == 1:
    channel_name = "Ch. A"
if channel_number == 2:
    channel_name = "Ch. B"
if channel_number == 3:
    channel_name = "Ch. C"

# Define the address for reading the buffer
address_start = 96
address_stop = 100
samples = 100

# Board settings
clock = 40.0e+6
dqqdidt_lsb = 2**-24
dqqdidt_a_lsb = 2**-24 / 2.5 # channel A
adc_speed = 205.128e+3
adc_set_value = int(clock / adc_speed)
sample_rate = clock / adc_set_value
f_resolution = sample_rate / samples

channel_a_range = 20 # +-10 Volt
channel_range = 50 # +- 25 Volt
channel_a_lsb = channel_a_range / (2**20)
channel_lsb = channel_range / (2**20)

# Define the COM port to use (see device manager setting on windows)
com_dqqdidt = 'COM22'

# Com port of the precise voltage source
com_pdvs2 = 'COM23'

# ID of the signal generator
sig_generator_id = 'USB0::0x0957::0x2807::MY52401786::INSTR'

# Test Paramaters
min_voltage = 10 # Volt
max_voltage = -10 # Volt
voltage_points = 41


####################### Setup #################################################
# Create port for communication
ser = serial.Serial()
ser2 = serial.Serial()

# Instanciate an UART communication object with the board
board_obj = UQDS(ser, com_dqqdidt)

# Precise voltage source handler
pdvs2_obj = PDVS2(ser2, com_pdvs2)

# Generate test stemps
test_voltages = np.linspace(min_voltage, max_voltage, voltage_points)


####################### Mesurements ###########################################
diff_list = np.array([])
measured_list = np.array([])
# volt_list_dmm = np.array([])
n = 0
voltage_negative = False

for volt in test_voltages:
    n = n + 1

    if volt < 0 and voltage_negative == False:
        voltage_negative = True
        print("Please change the board connection")
        if win_os == True:
            winsound.Beep(800, 1500)
        time.sleep(20)

    # PDVS2 set new voltage
    if voltage_negative == True:
        pdvs2_obj.generate_voltage(-volt)
    else:
        pdvs2_obj.generate_voltage(volt)

    # Reset board
    board_obj.reset()
    time.sleep(4)


    ####################### Loading new parameters ############################
    # Set ADC speed
    board_obj.write_register(12, [0x00, 0x00, 0x00, adc_set_value])
    time.sleep(10e-3)

    # Set readout config
    board_obj.write_register(10, [0x64, 0x00, 0x00, 0x00])
    time.sleep(10e-3)

    # Setting all channel config registers to zero to get unfiltered and undecimated data
    for i in range(19, 34):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)

    for i in range(128, 139):
        board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
        time.sleep(10e-3)

    time.sleep(1)


    ####################### Acquiring data ####################################
    board_obj.write_register(0x00, [0x13, 0x00, 0x00, 0x00])

    time.sleep(3)
    for i in range(0, 200):
        data = board_obj.read_register(i, 5)

        # if i == 7:
            # print("System status: " + str(data))

    time.sleep(3)
    for i in range(0, 200):
        data = board_obj.read_register(i, 5)

        # if i == 7:
            # print("System status: " + str(data))

        if i == 12:
            set_sps = data

    buffer = board_obj.read_buffer(samples, address_start, address_stop)
    # buffer = board_obj.trig_and_read_buffer(samples, adress_start, adress_stop, system_flag=0xea)

    # Multiply buffer values with LSB value
    # As channel A is different to B and C, a different LSB shall be used
    if channel_number == 1:
        buffer = np.multiply(buffer, dqqdidt_a_lsb)
    else:
        buffer = np.multiply(buffer, dqqdidt_lsb)

    data_ch_avg = np.average(buffer[:, channel_number - 1])
    measured_list = np.append(measured_list, data_ch_avg)

    # Calculate difference between measured signal and set signal
    voltage_diff = volt - data_ch_avg

    # Save data for later analysis
    diff_list = np.append(diff_list, voltage_diff)

    # Some nice output for the user
    done_percent = int(100 * (n / voltage_points))
    print("Measurement " + str(n) + "/" + str(voltage_points) + \
          " (" + str(done_percent) + "%)")
    print("Set: %.3f" % volt + " V")
    print("Meas.: %.3f" % data_ch_avg + " V")
    print("Difference: %.3f" % (voltage_diff * 1e+6)+ " uV")
    print("---------------------------")


# Close the connection to the physical devices
ser.close()
ser2.close()


####################### Plot ##################################################
plt.figure()
plt.plot(test_voltages, diff_list * 1e+6, '-*')
plt.grid(True)
plt.xlabel('Set Voltage (V)')
plt.ylabel('Difference (uV)')
plt.title(test_name + ", " + channel_name)
plt.tight_layout()
plt.savefig(path + linearity_filename)
